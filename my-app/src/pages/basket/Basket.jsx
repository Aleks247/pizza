import React from "react";
import './basket.css';
import Header from "../../component/header/Header";
import BasketCart from "../../component/basket-card/BasketCard";
import { NavLink } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { clearItem } from "../../redux/slices/basketSlice";

function Basket() {

	const dispatch = useDispatch();
	const totalPrice = useSelector((state) => state.basket.totalPrice);
	const items = useSelector((state) => state.basket.items);
	const totalCountInBasket = items.reduce((sum, items) => sum + items.count, 0);
	
	const onClearBasket = () => {
		dispatch(clearItem())
	}

	return (
		<div className="basket__position">
			<div className="basket">
				<div className="width">
					<Header></Header>
				</div>
				<div className="line"></div>
				<div className="width">
					{items < 1 ?
						(<div className="basket__content">
							<div className="basket__content__empty">
								<h2>Корзина пустая 😕</h2>
								<h3>Вероятней всего, вы не заказывали ещё пиццу.
								<p>Для того, чтобы заказать пиццу, перейди на главную страницу.</p></h3>
								<img src="img/basket/basket-content-empty.png" alt="basket empty" />
								<NavLink to={'/'}><button className="basket__button__back__empty"> Вернуться назад</button></NavLink>
							</div>
						</div>)
						:
						(<div className="basket__content">
							<div className="basket__content__top">
								<div className="basket__content__top__cart">
									<img src="img/basket/cart.svg" alt="cart" />
									<h2>Корзина</h2>
								</div>
								<div onClick={onClearBasket} className="basket__content__top__delete">
									<img src="img/basket/delete.svg" alt="delete" />
									<h3>Очистить корзину</h3>
								</div>
							</div>
							<div className="basket__overflow">
								{items.map((item) => (
									<BasketCart
										key={item.id}
										{...item}></BasketCart>
								))}
							</div>
							<div className="basket__info">
								<div className="basket__info__quantity">
									<h2>Всего пицц: <span>{totalCountInBasket} шт.</span> </h2>
								</div>
								<div className="basket__info__sum">
									<h2>Сумма заказа: <span>{totalPrice} ₽</span> </h2>
								</div>
							</div>
							<div className="basket__button">
								<NavLink to={'/'}><button className="basket__button__back"><img src="img/basket/back.svg" alt="back" /> Вернуться назад</button></NavLink>
								<button className="basket__button__pay">Оплатить сейчас</button>
							</div>
						</div>)
					}
				</div>
			</div>
		</div>
	)

}

export default Basket;
