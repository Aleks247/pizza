import React, { useState, useEffect, createContext } from "react";
import './home.css';
import Header from "../../component/header/Header";
import Card from "../../component/card/Card";
import axios from "axios";
import Skeleton from "../../component/skeleton/Skeleton";
import Category from "../../component/category/Category";
import Sort from "../../component/sort/Sort";
import Search from "../../component/search/Search";
import Pagination from "../../component/pagination/Pagination";
import { useSelector, useDispatch } from "react-redux";
import { setCategory, setSortItemActive } from "../../redux/slices/filterSlice";
import { setCurrentPage } from "../../redux/slices/paginationSlice";

export const MyContext = createContext();

function Home() {	
	const dispatch = useDispatch();
	const [pizzaItem, setPizzaItem] = useState([]);
	const [isLoading, setIsLoading] = useState(true);
	const [searchValue, setSearchValue] = useState('');
	const types = useSelector((state) => state.filter.category);
	const sortItem = useSelector((state) => state.filter.sortItem);
	const sortItemActive = useSelector((state) => state.filter.sort);
	const currentPage = useSelector((state) => state.pagination.currentPage);
	const itemsPerPage = useSelector((state) => state.pagination.itemsPerPage);
	const category = types > 0 ? `type=${types}` : '';
	const serch = searchValue ? `search=${searchValue}` : '';



	useEffect(() => {
		setIsLoading(true)
		axios.get(`https://642e6fc62b883abc640da793.mockapi.io/data?${category}${serch}
			`)
			.then(response => {
				setPizzaItem(response.data)
				setIsLoading(false)
			})
			.catch(error => {
				console.log(error)
			})
	}, [types, sortItemActive, searchValue, currentPage])

	const onClickCategory = (index) => {
		dispatch(setCategory(index))
		dispatch(setCurrentPage(1))
	}

	const sortadProducts = [...pizzaItem].sort((b, a) => {
		if (sortItemActive === 0) {
			return a.rating - b.rating;
		} if (sortItemActive === 1) {
			return b.rating - a.rating;
		} if (sortItemActive === 2) {
			return a.price - b.price;
		} if (sortItemActive === 3) {
			return b.price - a.price;
		} if (sortItemActive === 4) {
			return b.name.localeCompare(a.name)
		} if (sortItemActive === 5) {
			return a.name.localeCompare(b.name)
		}
	})

	const lastItemIndex = currentPage * itemsPerPage
	const firstItemIndex = lastItemIndex - itemsPerPage
	const currentItems = sortadProducts.slice(firstItemIndex, lastItemIndex)
	const paginate = pageNumber => dispatch(setCurrentPage(pageNumber))

	return (
		<MyContext.Provider value={{ sortItem, sortItemActive, setSortItemActive, types, searchValue, setSearchValue, itemsPerPage, paginate, currentPage, setCurrentPage, onClickCategory, pizzaItem, lastItemIndex, currentItems }}>
			<div className="home__position">
				<div className="home">
					<div className="width">
						<Header></Header>
					</div>
					<div className="line"></div>
					<div className="width">
						<div className="filtres">
							<Category></Category>
							<Sort></Sort>
						</div>
						<div className="catalog">
							<div className="catalog__search">
								<h1>Все пиццы</h1>
								<Search></Search>
							</div>
							<div className="catalog__content">
								{isLoading ? [...new Array(8)].map((_, index) => <Skeleton key={index}></Skeleton>) :
									currentItems.map((Object) => (
										<Card
											key={Object.id}
											{...Object}
										></Card>
									))}
							</div>
						</div>
						<Pagination totalItems={pizzaItem.length}></Pagination>
					</div>
				</div>
			</div>
		</MyContext.Provider>
	)

}

export default Home;