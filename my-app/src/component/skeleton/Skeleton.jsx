import React from "react"
import ContentLoader from "react-content-loader"

const Skeleton = (props) => (
  <ContentLoader 
    speed={2}
    width={280}
    height={470}
    viewBox="0 0 280 470"
    backgroundColor="#f3f3f3"
    foregroundColor="#ecebeb"
    {...props}
  >
    <circle cx="140" cy="140" r="140" /> 
    <rect x="0" y="327" rx="10" ry="10" width="280" height="85" /> 
    <rect x="136" y="419" rx="20" ry="20" width="140" height="42" /> 
    <rect x="0" y="431" rx="5" ry="5" width="108" height="23" /> 
    <rect x="0" y="290" rx="0" ry="0" width="280" height="24" />
  </ContentLoader>
)

export default Skeleton;