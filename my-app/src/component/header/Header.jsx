import React from "react";
import './header.css';
import { NavLink } from "react-router-dom";
import { useSelector } from "react-redux";

function Header() {

	const {items, totalPrice} = useSelector((state) => state.basket);
	const totalCount = items.reduce((sum, items) => sum + items.count, 0);

	return (
		<header>
			<NavLink to={'/'}>
				<div className="header__logo">
					<img src="img/header/logo.svg" alt="logo" />
					<div className="header__logo__txt">
						<h2>REACT PIZZA</h2>
						<h3>самая вкусная пицца во вселенной</h3>
					</div>
				</div>
			</NavLink>
			<NavLink to={'/basket'}>
				<button className="header__basket">
					<div><h3>{totalPrice} ₽</h3></div>
					<div className="header__basket__img"><img src="img/header/cart.svg" alt="cart" /> <h3>{totalCount}</h3></div>
				</button>
			</NavLink>
		</header>
	)

}

export default Header;