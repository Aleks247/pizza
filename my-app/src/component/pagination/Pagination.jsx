import React, { useContext, useState } from "react";
import './pagination.css';
import { MyContext } from "../../pages/home/Home";
import {setCurrentPage} from "../../redux/slices/paginationSlice";
import { useDispatch } from "react-redux";
import { plusPages, minusPages, equalsPages } from "../../redux/slices/paginationSlice";

function Pagination({ totalItems }) {

	const dispatch = useDispatch();

	const { itemsPerPage, paginate, currentPage, lastItemIndex, currentItems } = useContext(MyContext)

	const pageNumbers = []

	for (let i = 1; i <= Math.ceil(totalItems / itemsPerPage); i++) {
		pageNumbers.push(i)
	}

	const nextPage = () => {
		if (currentPage < lastItemIndex) {
			dispatch(plusPages())
		} if (currentItems.length < itemsPerPage) {
			dispatch(equalsPages())
		}
	}

	const prevPage = () => {
		if (currentPage > 1) {
			dispatch(minusPages())
		}
	}

	return (
		<div>
			<ul className="pagination">
				<button
					className="pagination__button pagination__button__prev"
					onClick={prevPage}
				>&#10094;&#10094;</button>
				{pageNumbers.map((number) => (
					<li
						className={currentPage === number ? "pagination__item active" : "pagination__item"}
						key={number}
						onClick={() => paginate(number)}
					>{number}</li>
				))
				}
				<button
					className="pagination__button pagination__button__next"
					onClick={nextPage}
				>&#10095;&#10095;</button>
			</ul>
		</div>
	)

}

export default Pagination;