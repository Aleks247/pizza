import React, {useContext, useState} from "react";
import './sort.css';
import { MyContext } from "../../pages/home/Home";
import { useDispatch } from "react-redux";

function Sort() {

	const dispatch = useDispatch();
	const {sortItem, sortItemActive, setSortItemActive} = useContext(MyContext);
	const [sortItemOpen, setSortItemOpen] = useState(false);


	const openSort = (option) => {
		dispatch(setSortItemActive(option))
		setSortItemOpen(false);
	}

	return (
		<div className="sort">
			<h3><img className={sortItemOpen ? "sort__img active" : "sort__img"} src="img/sort/arrow.svg" alt="arrow" /> Сортировка по: <span onClick={() => setSortItemOpen(!sortItemOpen)}>{sortItem[sortItemActive]}</span></h3>
			<div className={sortItemOpen ? "sort__item active" : "sort__item"}>
				<ul>
					{sortItem.map((sortItem, option) => (
						<li key={option} onClick={() => openSort(option)} className={sortItemActive === option ? "sort__item__li active" : "sort__item__li"}>{sortItem}</li>
					))}
				</ul>
			</div>
		</div>
	)

}

export default Sort;