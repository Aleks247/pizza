import React from "react";
import './basketCard.css';
import { useDispatch, useSelector } from "react-redux";
import { addInBasket, removeInBasket, minusItem } from "../../redux/slices/basketSlice";

function BasketCart({imageUrl, name, doughs, sizes, price, id, count}) {

	const dispatch = useDispatch();
	const onClickPlus = () => {
		dispatch(addInBasket({
			id
		}))
	}
	const onClickMinus = () => {
		dispatch(minusItem(id))
	}
	const onClickRemove = () => {
		dispatch(removeInBasket(id))
	}
	return (
		<div className="basket__cart__box">
			<div className="basket__cart">
				<div className="basket__cart__name">
					<img src={imageUrl} alt="pizza" />
					<div className="basket__cart__name__txt">
						<h2>{name}</h2>
						<h3>{doughs}, {sizes} см.</h3>
					</div>
				</div>
				<div className="basket__cart__quantity">
					<button onClick={onClickMinus}><img src="img/basket/minus.svg" alt="minus" /></button>
					<h2>{count}</h2>
					<button onClick={onClickPlus}><img src="img/basket/plus.svg" alt="plus" /></button>
				</div>
				<div className="basket__cart__price">
					<h2>{price * count} ₽</h2>
				</div>
				<button onClick={onClickRemove} className="button__delete"><img src="img/basket/close.svg" alt="close" /></button>
			</div>
		</div>
	)

}

export default BasketCart;
