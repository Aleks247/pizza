import React, { useCallback, useState } from "react";
import './search.css';
import { useContext } from "react";
import { MyContext } from "../../pages/home/Home";
import {debounce} from 'lodash';

function Search() {

	const [value, setValue] = useState('')
	const { searchValue, setSearchValue, pizzaItem } = useContext(MyContext)
	const [ isOpenItemSearch, setIsOpenItemSearch] = useState(true);

	const debounceOnChangeSearchValue = useCallback(
		debounce((str) => {
			setSearchValue(str);
		}, 250),
		[]
	)

	const onChangeSearchValue = (e) => {
		setValue(e.target.value)
		debounceOnChangeSearchValue(e.target.value)
	}

	const onClickSearch = () => {
		setIsOpenItemSearch(true)
	}
	const itemClickSearch = (e) => {
		setSearchValue(e.target.textContent)
		setValue(e.target.textContent)
		setIsOpenItemSearch(false)
	}

	return (
		<div className="serch__block">
			<input
				value={value}
				onChange={onChangeSearchValue}
				className="search"
				type="text"
				placeholder="Поиск..." 
				onClick={onClickSearch}/>
			{searchValue && isOpenItemSearch ?
				<ul className="search__window">
				{pizzaItem.map((Object, index) => (
					<li
						key={index}
						className="search__window__item"
						{...Object}
						onClick={itemClickSearch}
					>{Object.name}</li>
					))}
				</ul> :
				''
			}
		</div>
	)

}

export default Search;