import React, {useContext, useState} from "react";
import './category.css';
import { MyContext } from "../../pages/home/Home";

function Category() {

	const { types, setCurrentPage, onClickCategory} = useContext(MyContext)

	const type = ['Все', 'Мясные', 'Вегетарианская', 'Гриль', 'Острые', 'Закрытые'];
 return(
	<div className="type">
	<ul>
		{type.map((type, index) => (
			<li key={index} onClick={() => onClickCategory(index)} className={types === index ? "type__item active" : "type__item"}>{type}</li>
		))}
	</ul>
</div>
 )

}

export default Category;