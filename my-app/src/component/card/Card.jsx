import React, { useState } from "react";
import './card.css';
import { useDispatch, useSelector } from "react-redux";
import { addInBasket } from '../../redux/slices/basketSlice';

function Card({ imageUrl, name, dough, size, price, id }) {

	const dispatch = useDispatch();
	const itemCount = useSelector((state) => state.basket.items.find(Object => Object.id === id));
	const [choiceDough, setChoiceDough] = useState(0);
	const [choiceSize, setChoiceSize] = useState(0);
	const choiceDoughItem = ['тонкое', 'традиционное'];
	const [hoverBtn, setHoverBtn] =useState(false);
	const addCount = itemCount ? itemCount.count: 0;
	
	const onMouseEnter = () => {
		setHoverBtn(true)
	}

	const onMouseLeave = () => {
		setHoverBtn(false)
	}

	const onAddBasket = () => {
		const itemForBasket = {
			id,
			name,
			imageUrl,
			price,
			doughs: choiceDoughItem[choiceDough],
			sizes: size[choiceSize]
		};
		dispatch(addInBasket(itemForBasket));
	}

	return (
		<div className="card">
			<img src={imageUrl} alt="pizza" />
			<h2>{name}</h2>
			<div className="choice">
				<div className="choice__dough">
					<ul>
						{dough.map((dough) => (
							<li key={dough} onClick={() => setChoiceDough(dough)} className={choiceDough === dough ? "choice__dough active" : "choice__dough"}>{choiceDoughItem[dough]}</li>
						))}
					</ul>
				</div>
				<div className="choice__size">
					<ul>
						{size.map((size, index) => (
							<li key={size} onClick={() => setChoiceSize(index)} className={choiceSize === index ? "choice__size active" : "choice__size"}>{size} см.</li>
						))}
					</ul>
				</div>
			</div>
			<div className="card__button">
				<h2>от {price} ₽</h2>
				<button
					onClick={onAddBasket}
					onMouseEnter={onMouseEnter}
					onMouseLeave={onMouseLeave}
				><img src={hoverBtn ? "img/card/plus-active.svg" : "img/card/plus.svg"} alt="plus" /> Добавить
					{addCount > 0 && <div className="card__button__quantity">{addCount}</div>}
				</button>

			</div>
		</div>
	)

}

export default Card;