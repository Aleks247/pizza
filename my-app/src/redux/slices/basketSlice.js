import { createSlice } from "@reduxjs/toolkit"

const initialState = {
	totalPrice: 0,
	items: [],	
}

const basketSlice = createSlice({
	name: 'basket',
	initialState,
	reducers: {
		// addInBasket(state, action){
		// 	state.items.push(action.payload);
		// 	state.totalPrice = state.items.reduce((sum, Object) => {
		// 		return parseInt(Object.price) + sum;
		// 	}, 0)
		// },
		addInBasket(state, action){
			const findItem = state.items.find(Object => Object.id === action.payload.id);
			if(findItem){
				findItem.count++;
			} else{
				state.items.push({
					...action.payload, 
					count: 1,
				});
			}
				state.totalPrice = state.items.reduce((sum, Object) => {
					return (Object.price * Object.count) + sum;
				}, 0)
		},
		removeInBasket(state, action){
			state.items = state.items.filter(Object => Object.id !== action.payload);

			state.totalPrice = state.items.reduce((sum, Object) => {
				return (Object.price * Object.count) + sum;
			}, 0)
		},
		clearItem(state) {
			state.items = []
			state.totalPrice = 0
		}, 
		minusItem(state, action){
			const findItem = state.items.find(Object => Object.id === action.payload);

			if (findItem){
				findItem.count--;
			} if (findItem.count < 1) {
				state.items = state.items.filter(Object => Object.id !== action.payload);
			}

			state.totalPrice = state.items.reduce((sum, Object) => {
				return (Object.price * Object.count) + sum;
			}, 0)
		}
	}
})

export const { addInBasket, removeInBasket, clearItem, minusItem } = basketSlice.actions;

export default basketSlice.reducer;
