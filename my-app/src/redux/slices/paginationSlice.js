import { createSlice } from "@reduxjs/toolkit";

const initialState = {
	currentPage: 1,
	itemsPerPage: 8,
}

const paginationSlice = createSlice({
	name: 'paginations',
	initialState,
	reducers: {
		setCurrentPage(state, action){
			state.currentPage = action.payload;
		}, 
		plusPages(state){
			state.currentPage += 1;
		} ,
		minusPages(state){
			state.currentPage -= 1;
		  },
		equalsPages(state, action){
			state.currentPage = 1;
		}
	}
})

export const {setCurrentPage, plusPages, minusPages, equalsPages} = paginationSlice.actions;

export default paginationSlice.reducer;