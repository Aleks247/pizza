import { createSlice } from "@reduxjs/toolkit"

const initialState = {
	category: 0,
	sort: 0,
	sortItem: ['max-популярности', 'min-популярности', 'max-цене', 'min-цене', 'алфавиту а-я', 'алфавиту я-а'],
}

const filterSlice = createSlice({
	name: 'filter',
	initialState,
	reducers: {
		setCategory(state, action) {
			state.category = action.payload;
		},
		setSortItemActive(state, action) {
			state.sort = action.payload
		}
	}
})

export const { setCategory, setSortItemActive } = filterSlice.actions;

export default filterSlice.reducer;
