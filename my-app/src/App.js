import './App.css';
import {BrowserRouter as Router, Routes, Route} from 'react-router-dom';
import Home from './pages/home/Home';
import Basket from './pages/basket/Basket';

function App() {
  return (
   <Routes>
		<Route path='/' element={<Home></Home>}></Route>
		<Route path='/basket' element={<Basket></Basket>}></Route>
   </Routes>
  );
}

export default App;
